function seleccionar(){
    $("#chk_op1").on('change', function (ev){
        ev.preventDefault();//Cancela el evento si es cancelable, pero no detiene el resto del funcionamiento del evento, es decir puede ser llamado de nuevo
        $("chk_op1").prop("checked", true)
        $("chk_op2").prop("checked", false)
        $("chk_op3").prop("checked", false)
        $("chk_op4").prop("checked", false)

        $("#errores").html("Solo dos check puede ser seleccionados.")
        setTimeout(function(){
            $('errores').html("");
        }, 2000); //valor es 2000 milisegundos = 2 segundos
    });

    $("#chk_op2").on('change', function (ev){
        ev.preventDefault();//Cancela el evento si es cancelable, pero no detiene el resto del funcionamiento del evento, es decir puede ser llamado de nuevo
        $("chk_op2").prop("checked", true)
        $("chk_op1").prop("checked", false)
        $("chk_op3").prop("checked", false)
        $("chk_op4").prop("checked", false)

        $("#errores").html("Solo dos check puede ser seleccionados.")
        setTimeout(function(){
            $('errores').html("");
        }, 2000); //valor es 2000 milisegundos = 2 segundos
    });

    $("#chk_op3").on('change', function (ev){
        ev.preventDefault();//Cancela el evento si es cancelable, pero no detiene el resto del funcionamiento del evento, es decir puede ser llamado de nuevo
        $("chk_op3").prop("checked", true)
        $("chk_op1").prop("checked", false)
        $("chk_op2").prop("checked", false)
        $("chk_op4").prop("checked", false)

        $("#errores").html("Solo dos check puede ser seleccionados.")
        setTimeout(function(){
            $('errores').html("");
        }, 2000); //valor es 2000 milisegundos = 2 segundos
    });

    $("#chk_op4").on('change', function (ev){
        ev.preventDefault();//Cancela el evento si es cancelable, pero no detiene el resto del funcionamiento del evento, es decir puede ser llamado de nuevo
        $("chk_op4").prop("checked", true)
        $("chk_op1").prop("checked", false)
        $("chk_op2").prop("checked", false)
        $("chk_op3").prop("checked", false)

        $("#errores").html("Solo dos check puede ser seleccionados.")
        setTimeout(function(){
            $('#errores').html("");
        }, 2000); //valor es 2000 milisegundos = 2 segundos
    });
}